function bai_1() {
  var n = 0;
  var sum = 0;
  while (sum < 10000) {
    n++;
    sum += n;
  }
  document.getElementById("b1-ket-qua").innerText = "Giá trị của n: " + n;
}

function bai_2() {
  var x = document.getElementById("b2-x").value * 1;
  var n = document.getElementById("b2-n").value * 1;
  var ket_qua = 0;
  for (i = 1; i <= n; i++) {
    ket_qua += Math.pow(x, i);
  }
  document.getElementById("b2-tinh-tong").innerText = "Tổng: " + ket_qua;
}

function bai_3() {
  var n = document.getElementById("b3-n").value * 1;
  var m = 1;
  for (var i = 1; i <= n; i++) {
    m *= i;
  }
  document.getElementById("b3-giai-thua").innerText = "Giai thừa: " + m;
}

function bai_4() {
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      document.getElementById(
        "b4-tao-the"
      ).innerHTML += `<div class="bg-danger text-white">Div Chẵn ${i}</div>`;
    } else {
      document.getElementById(
        "b4-tao-the"
      ).innerHTML += `<div class="bg-primary text-white">Div Lẻ ${i}</div>`;
    }
  }
}
